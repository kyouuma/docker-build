import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Avaxia Group Demo Application
        </p>
        <p>added JIRA-37</p>
        <a
          className="App-link"
          href="https://www.avaxiagroup.com/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn less
        </a>
      </header>
    </div>
  );
}

export default App;
